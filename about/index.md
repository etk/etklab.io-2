---
layout: page
title: About Tama g3tux
tags: [about, TuxLabs]
modified: 2015-05-17T11:00:00.12345-05:30
comments: true
image:
  feature: https://gitlab.com/etk/file/raw/master/public/gaptek.png
  credit: gaptek.net
  creditlink: http://gaptek.net
---
<!-- Redirect users to Resume page-->
<script type="text/javascript">
  window.location = window.location.toString().replace(/about/, "resume");
</script>

Tama g3tux passionate about making things easy/automate using shell scripts. <br>
I believe in "Stay Lazy" so most of my task is replaced by small scripts :)

## Skills

* Linux
* NGINX
* Ansible
* ELK Stack
* Vagrant/Docker
* Debian/Ubuntu
* Redhat/CentOS
* Shell Scripting
* Computer Security
* System Administrator
