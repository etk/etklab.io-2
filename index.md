---
layout: home
excerpt: "Tama g3tux Personal Blog - All about Linux and System Admin stuff."
tags: [TuxLabs, Linux, System Admin, Geek, Coder]
image:
  feature: https://gitlab.com/etk/file/raw/master/public/gaptek.png
  credit: gaptek.net
  creditlink: http://gaptek.net
---
