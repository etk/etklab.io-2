---
layout: page
title: Linux, NGINX, WordPress Expert Resume
tags: [TuxLabs, Resume, Curriculum Vitae, CV]
excerpt: "Linux Enthusiast"
modified: 2018-08-29T17:12:42.12345-05:30
comments:
blockads: true
image:
  feature: https://gitlab.com/etk/file/raw/master/public/gaptek.png
  credit:
  creditlink:
---


#### Resume is being under construction :p

I am passionate about Linux and Open Source software.<br>
I like to create reliable well-automated systems, that require minimal manual mainteinance.
A good sysadmin is a lazy sysadmin :)
<br><br>
